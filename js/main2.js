/*1. Diseña una función que reciba como argumento un arreglo de valores
enteros de 20 posiciones , regrese el valor promedio de los elementos del arreglo*/


//Funcion para los valores enteros
function aleatorios(){
    return Math.floor((Math.random()*(50 - 1 + 1)) + 1);
}

    var arreglo1=Array(20);
    for(let i=0; i<20; i++){
        arreglo1[i]=aleatorios();
    }

console.log(arreglo1);
var conteo=0;
function valorPromedio(arreglo1){
 
    for(let i=0; i<20; i++){
         conteo = conteo + arreglo1[i];
    }
    return conteo/20;
    
}

// Impresión del promedio
console.log("El valor promedio es: " + valorPromedio(arreglo1));



/*2. Diseñe una función que reciba como argumento un arreglo de 20 valores
numéricos enteros, y me regrese la cantidad de valores pares que existe en el
arreglo*/

var pares=0;

function conteoPares(arreglo1){
    for(let i=0; i<20; i++){
        if (( arreglo1[i] % 2) == 0) {
            pares = pares + 1;
        }
   }
   return pares;
}

// Impresion de los numeros pares
console.log("Los numeros pares son: " + conteoPares(arreglo1));


/* 3. Diseñe una función que reciba como argumento un arreglo de 20 valores
numéricos enteros, ordene los valores del arreglo de mayor a menor. */


function orden(arreglo1){
    arreglo1.sort(function(a, b) {return b - a;});
      return arreglo1;
}
// Impresion de valores ordenados
console.log(orden(arreglo1));


function Mostrar(){
    //Mostrar arreglo
    let parrafo = document.getElementById('arreglo');

    parrafo.innerHTML = null;
    
    for(let con=1; con<20;con++){
       parrafo.innerHTML = parrafo.innerHTML+arreglo1[con]+" ";
    }
    //mostrar resultado 1
    var op1=document.getElementById('uno');
    var mod = valorPromedio(arreglo1);
    op1.innerHTML=null;
    op1.innerHTML="El valor de la función uno es "+(mod-valorPromedio(arreglo1));
    
    //Mostrar resultado 2
    var op2 = document.getElementById('dos');
    var mod2 = conteoPares(arreglo1);
    op2.innerHTML=null;
    op2.innerHTML="El valor de la funcion dos es "+(mod2-conteoPares(arreglo1));


    //Mostrar resultado 3
    let orden11=orden(arreglo1);
    let parrafo2 = document.getElementById('tres');

    parrafo2.innerHTML = null;
    
    parrafo2.innerHTML = parrafo2.innerHTML+orden11+" ";
  


}
